import getAllDivisors from './getAllDivisors.js';
import getSquaredTotalSum from './getSquaredTotalSum.js';
import getSquareRoot from './getSquareRoot.js';

export { getAllDivisors, getSquaredTotalSum, getSquareRoot };
