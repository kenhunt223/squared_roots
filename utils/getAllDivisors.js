const getAllDivisors = (num) => {
  const numbers = [1, num];
  const half = num / 2;
  let currentIterator = 2;

  // * We need only whole numbers
  if (half % 1 === 0) numbers.push(half);

  while (currentIterator < half) {
    if (num % currentIterator === 0) numbers.push(currentIterator);

    currentIterator++;
  }

  return numbers.sort((a, b) => a - b);
};

export default getAllDivisors;
