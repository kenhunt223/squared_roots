const getSquareRoot = (num) => {
  const squareRoot = Math.sqrt(num);

  if (squareRoot % 1 === 0) return squareRoot;
  else return 0;
};

export default getSquareRoot;
