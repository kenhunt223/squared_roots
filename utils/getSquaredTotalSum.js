const getSquaredTotalSum = (numbers) => {
  let totalSum = 0;

  numbers.forEach((num) => (totalSum += num * num));

  return totalSum;
};

export default getSquaredTotalSum;
