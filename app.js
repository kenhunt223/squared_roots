import { getAllDivisors, getSquaredTotalSum, getSquareRoot } from './utils/index.js';

const getAllSquareRoots = (x, y) => {
  const allNumbers = [];
  let currentIterator = x;

  while (currentIterator <= y) {
    const allDivisors = getAllDivisors(currentIterator);
    const squaredTotalSum = getSquaredTotalSum(allDivisors);
    const squaredRoot = getSquareRoot(squaredTotalSum);

    if (!!squaredRoot) allNumbers.push(currentIterator);

    currentIterator++;
  }

  return allNumbers;
};

// * The purpose of the function is to iterate through the range x -> y and:
// *    - find all the divisors of the number (whole numbers only)
// *    - find the total Sum of [each divisor multiplied by itself] then [added together].
// *    - find the sum with a square root as whole number.
const allSquareRoots = getAllSquareRoots(2, 2873);

console.log(allSquareRoots);
